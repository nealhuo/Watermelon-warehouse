from __future__ import absolute_import, unicode_literals
import os
from celery import Celery

# set the default Django settings module for the 'celery' program.
# 让celery了解当前的环境变量
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'SmartMan.settings')

app = Celery('SmartMan')

# Using a string here means the worker don't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
# 会在settings中找celery的配置，所以连接redis等配置都要写到settings中
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks() #每个app中如果有celery的任务，celery就会自动识别


@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))