"""SmartMan URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from smart import views


urlpatterns = [
    path('admin/', admin.site.urls),
    path(r'', views.index),
    path(r'test/', views.test),
    path(r'time/', views.time),
    path(r'login/', views.login),
    # 用CBV方式处理请求：
    # path(r'login/', views.Login.as_view()),
    path(r'logout/', views.logout),
    path(r'dashboard/', views.dashboard),
    path(r'device/', views.device),
    path(r'inspect/', views.inspect),
    path(r'audit/', views.audit),
    path(r'help/', views.help),
    path(r'configuration/', views.config),
    path(r'multiconfig/', views.multi_config),
    path(r'celery_test/', views.test),

]
