from django.db import models


# Create your models here.

class Administrator(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    username = models.TextField()  # This field type is a guess.
    password = models.TextField()  # This field type is a guess.
    email = models.TextField(blank=True, null=True)  # This field type is a guess.
    department = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'administrator'


class Device(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    mng_ip = models.TextField()  # This field type is a guess.
    username = models.TextField()  # This field type is a guess.
    password = models.TextField(blank=True, null=True)  # This field type is a guess.
    manufacture = models.TextField(blank=True, null=True)  # This field type is a guess.
    owner = models.TextField(blank=True, null=True)  # This field type is a guess.
    hostname = models.TextField(blank=True, null=True)  # This field type is a guess.
    def __str__(self):
        return self.mng_ip
    class Meta:
        managed = False
        db_table = 'device'


class Student(models.Model):
    id = models.IntegerField(primary_key=True, blank=True)  # AutoField?
    name = models.TextField()  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'student'

    def __unicode__(self):
        return self.name
