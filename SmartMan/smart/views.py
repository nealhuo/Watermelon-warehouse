from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from smart import models
from .utilis import log
from .device import validate_connection, get_basic_info, get_device_sys
# from django import views 用于CBV
import datetime
import json
from smart import tasks

# Create your views here.
class Person(object):
    def __init__(self, name, age, sex):
        self.name = name
        self.age = age
        self.sex = sex

    def sayhi(self):
        return 'my name is {}'.format(self.name)

    '''
    用CBV的方式来处理请求。views.View类中已经定义好了各种http请求方法，
    我们可以直接定义到这个类中，优点是不用再对各种request.method进行判断，
    缺点是可读性差，而且装饰器的使用很麻烦。
    
class Login(views.View):
    def get(self, request, *args, **kwargs):

        return HttpResponseRedirect('/')


    def post(self, request, *args, **kwargs):
        username = request.POST.get('username')
        password = request.POST.get('password')
        # 查表：在Administrator这张表中查找这条记录出现的次数
        result = models.Administrator.objects.filter(username=username, password=password).count()
        if result:
            request.session['username'] = username
            request.session['is_login'] = True
            rep = HttpResponseRedirect('/main')
            # 给客户机返回response时候添加一个cookie，设置cookie存活时间为300秒
            # set_cookie是未加密的cookie：
            # rep.set_cookie('username', username, max_age=300)
            # 可以用签名的cookie，用signed方法给cookie加密，但解密也得用signed方法
            # rep.set_signed_cookie('username', username)
            return rep
        return HttpResponse('<h1>用户名密码错误</h1>')
        pass
        '''


def index(request):
    t = loader.get_template('login.html')  # 获取一个模板
    # c = Context({})  #向模板提供数据
    return HttpResponse(t.render())  # 没有需要替换的变量


def login(request):
    if request.method == 'POST':
        # request.POST是一个存放http body中的键值对的字典,用get方法取值
        username = request.POST.get('username')
        password = request.POST.get('password')
        # 查表：在Administrator这张表中查找这条记录出现的次数
        result = models.Administrator.objects.filter(username=username, password=password).count()
        if result:
            rep = HttpResponseRedirect('/dashboard')
            request.session['username'] = username
            request.session['is_login'] = True
            # 给客户机返回response时候添加一个cookie，设置cookie存活时间为300秒
            # set_cookie是未加密的cookie：这里就先不使用了，咱们用session
            # rep.set_cookie('username', username, max_age=300)
            # 可以用签名的cookie，用signed方法给cookie加密，但解密也得用signed方法
            # rep.set_signed_cookie('username', username)

            return rep
        return HttpResponse('<h1>用户名密码错误</h1>')
    else:
        # r如果不是post请求，那么就跳转会首页
        return HttpResponseRedirect('/')


def logout(request):
    if request.method == 'POST':
        rep = HttpResponseRedirect('/')
        rep.delete_cookie('username')
        return rep
    else:
        return HttpResponse('<h1>this action should is GET!!Not POST!! </h1>')


def dashboard(request):
    # 判断用户是否登录，如果登录，则获取用户名
    # 如果没有登录，则重定向到login页面
    # username = request.COOKIES.get('username')
    username = request.session.get('username')
    t = loader.get_template('dashboard.html')
    if username:
        return HttpResponse(t.render({'username': username}))
    else:
        return HttpResponseRedirect('/')


def device(request):
    if request.method == 'GET':
        username = request.session.get('username')
        # 获取所有的设备列表
        dev_list = models.Device.objects.all()
        t = loader.get_template('device.html')
        if username:
            return HttpResponse(t.render({'username': username, 'dev_list': dev_list}))
        else:
            return HttpResponseRedirect('/')

    elif request.method == 'POST':
        mng_ip = request.POST.get('mng_ip')
        username = request.POST.get('username')
        password = request.POST.get('password')
        owner = request.POST.get('owner')
        manufacture = request.POST.get('manufacture')

        device_sys = get_device_sys(manufacture)

        response_dict = {'status': True, 'error': None, 'data': None}

        try:  # 判断库内是否已经存在这台设备 TODO 改掉这坨异常处理，用条件判断的方式处理
            models.Device.objects.get(mng_ip=mng_ip)
            response_dict['status'] = False
            response_dict['error'] = '{} 已经存在，请确认'.format(mng_ip)
            response_jason = json.dumps(response_dict)
            return HttpResponse(response_jason)

        except:

            result, nc = validate_connection(mng_ip, username, password, device_sys)
            log(result, nc)
            if result:
                info = get_basic_info(nc)
                # info 值为 (['\n3.3.3.3\n'], ['R3-VMX'], ['14.1R1.10'])
                hostname = info[1][0]
                log(hostname)
                obj = models.Device.objects.create(
                    mng_ip=mng_ip,
                    username=username,
                    password=password,
                    owner=owner,
                    manufacture=manufacture,
                    hostname=hostname,
                )

                dev_obj = models.Device.objects.get(mng_ip=mng_ip)

                response_dict['data'] = {
                    'mng_ip': mng_ip,
                    'username': username,
                    'owner': owner,
                    'manufacture': manufacture,
                    'hostname': hostname,
                    'id': dev_obj.id,
                }
                print(response_dict)
                # print(obj.id)取不到id，如果能取到id，就可以通过字典返回给前端了。可能是这个版本的django行为变了
                # print(obj.ip_address)
                # return HttpResponseRedirect('/device')
                response_jason = json.dumps(response_dict)
            else:
                response_dict['status'] = False
                log(type(nc))
                response_dict['error'] = nc
                response_jason = json.dumps(response_dict)
    return HttpResponse(response_jason)


def inspect(request):
    username = request.session.get('username')
    t = loader.get_template('inspection.html')
    if username:
        return HttpResponse(t.render({'username': username}))
    else:
        return HttpResponseRedirect('/')


def audit(request):
    username = request.session.get('username')
    t = loader.get_template('audit.html')
    if username:
        return HttpResponse(t.render({'username': username}))
    else:
        return HttpResponseRedirect('/')


def help(request):
    username = request.session.get('username')
    t = loader.get_template('help.html')
    if username:
        return HttpResponse(t.render({'username': username}))
    else:
        return HttpResponseRedirect('/')


def config(request):
    username = request.session.get('username')
    t = loader.get_template('configuration.html')
    if username:
        return HttpResponse(t.render({'username': username}))
    else:
        return HttpResponseRedirect('/')

def multi_config(request):
    username = request.session.get('username')
    t = loader.get_template('multi_config.html')
    if username:
        return HttpResponse(t.render({'username': username}))
    else:
        return HttpResponseRedirect('/')

def test(request):
    t = loader.get_template('test.html')
    # 传递的参数有str，list，字典，对象，这些都可以再模板中显示出来
    tom = {'name': 'Tom', 'age': 18}
    person = Person('zhanghua', 26, 'girl')
    # booklist = []
    booklist = ['哈利波特', '计算机网络', 'openstack详解', 'django基础']
    args = {'title': 'haha', 'name': 'Neal', 'user': tom, 'person': person, 'booklist': booklist}
    return HttpResponse(t.render(args))


def time(request):
    # 获取http请求中的参数并显示
    id = request.GET['id']
    name = request.Get.get('name')
    now_time = datetime.datetime.now()
    now_time = now_time.year
    t = loader.get_template('time.html')
    return HttpResponse(t.render({'today': now_time, 'id': id, 'name': name}))


def test(request):
    res = tasks.add.delay(1,1)
    print('result = {}'.format(res))
    return HttpResponse('the task is running, the task id = {}'.format(res))
