from django.contrib import admin
from smart.models import Student, Administrator, Device
# Register your models here.
#把student这个表同步到contrib数据库
admin.site.register(Student)
admin.site.register(Administrator)
admin.site.register(Device)