# Create your tasks here
from __future__ import absolute_import, unicode_literals
from celery import shared_task
import time
from smart.semail import sendmsg


@shared_task  # 这个装饰器的作用是允许各个app都可以导入这个任务
def add(x, y):
    stime = time.ctime()
    print(stime)
    time.sleep(20)
    etime = time.ctime()
    print(etime)
    res = '{}. PS: The task started in {}, finish in {}'.format(x+y, stime, etime)
    sendmsg(res)
    return None


@shared_task
def mul(x, y):
    time.sleep(10)
    return x * y


@shared_task
def xsum(numbers):
    time.sleep(10)
    return sum(numbers)