# -*- coding: utf-8 -*-
import time


def log(*args, **kwargs):
    fmat = '%Y/%m/%d %H:%M:%S'
    value = time.localtime(int(time.time()))
    dt = time.strftime(fmat, value)
    print('Log:', dt, '\n', *args, **kwargs)
    return


if __name__ == '__main__':
    log('hh')
